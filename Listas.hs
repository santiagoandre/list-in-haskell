module Listas
( add'
, modifyElement
, len'
, quicksort'
, filtrer'
, head'
)where

-- aniade un elemento al final de una lista
      add':: [e] -> e -> [e]
      add'  l e = l ++ [e]

      -- modifica los elementos de una lista de acuerdo a una funcion que determina como mdificar cada elemento
      -- ella vera cuando y cuales modificaciones hacer
      modifyElement:: [e] ->(e->e) -> [e]
      modifyElement [] _ = []
      modifyElement (e:s) modifier = (modifier e):modifyElement s modifier

      {-}
      -- elimina elemenos si este cumple una condicion
      deleteElementsIf:: [e]  -> (e->Bool) -> [e]
      deleteElementsIf []  _ = []
      deleteElementsIf (e:s) compare' = if compare' e
                                          then deleteElementsIf s compare' -- No tengo en cuenta el elemento e
                                          else e: deleteElementsIf s compare'
      -}
      len':: [e] -> Integer
      len' [] = 0
      len' (_:s) = 1 + len' s

      quicksort' ::[a] ->(a -> a -> Ordering) -> [a]
      quicksort' [] _ = []
      quicksort' (x:xs) compareE =
          let smallerSorted = quicksort' [a | a <- xs, compareE a x /= GT] compareE
              biggerSorted  = quicksort' [a | a <- xs, compareE a x == GT] compareE
          in  smallerSorted ++ [x] ++ biggerSorted

      -- filtra los elementos de una lista de acuerdo a una funcion de fitracion
      filtrer' :: [a] -> (a -> Bool) -> [a]
      filtrer'  [] _ = []
      filtrer'  (e:s) compare' = if compare' e
                                    then e:(filtrer' s compare')
                                    else filtrer' s compare'
      --obtiene el elemento i de una lista
      {-
      getElement:: [e] -> Int -> e
      getElement [] _ = error "List empty."
      getElement (x:_) 0 = x
      getElement (_:s) i = getElement s (i-1)
      -}
      -- obtiene el primer elemento de una lista
      head':: [e] -> e
      head' [] = error "List is empty"
      head' (e:_) = e
