module Menu
( iterateForever
, selectOption
, printMenu
, getChar'
) where

import System.IO


--import Control.Exception
{-Esta funcion recive:
      - Una lista de String que imprime.
      - Una lista de funciones  que procesan la opcion elegida por el usuario
      - Algo que es procesado
 Y retorna un evento vacio
-}


iterateForever:: [String] ->[Char] -> [e -> IO e]-> e-> IO ()
iterateForever options indexs menuActions thing = do
       opc <-  selectOption options indexs
       let menuAction = getMenuAction menuActions indexs opc
       thing' <-menuAction thing
       iterateForever options indexs menuActions thing'
-- printMenu recive una lista de opciones que va a imprimir y el numero de la primera opcion.
printMenu::(Show a, Show b) => [b] -> [a] -> IO ()

printMenu [] _ = do return ()
printMenu _ [] = putStr "----------"
printMenu (x:s) (w:y) = do
      let opc  = (show w) ++ ". "++ show x ++ ".\n"
      putStr opc
      printMenu s y
selectOption:: [String] -> [Char] -> IO Char
selectOption options indexs = do
      printMenu options indexs
      opc <- getChar'
      if opc `elem` indexs
            then return opc
            else do
                  putStr "Opcion invalida"
                  selectOption options indexs

getChar' :: IO Char
getChar' =  do
      hSetBuffering stdin NoBuffering
      c <- getChar
      putStr "\n"
      hSetBuffering stdin LineBuffering
      return c

getMenuAction::(Ord s) => [e -> IO e] -> [s] -> s -> (e -> IO e)
getMenuAction _ [] _ =  invalidOption
getMenuAction [] _  _ = invalidOption
getMenuAction (x:s) (a:b)  c
            | a > c  = invalidOption
            | a == c = x
            | otherwise = getMenuAction s b c


invalidOption:: e -> IO e
invalidOption thing = do
      putStr "Opcion invalida mi pez \n."
      return thing-- al ser la ultima accion IO el do la toma como valor de retorno
