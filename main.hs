import System.IO()
import System.Exit
import Menu
import Listas
-- tipos


data Genero = Femenino | Masculino | Otro deriving Show
data DiagnosticoIMC = IN | SN | O | N deriving (Eq,Ord)
instance Show DiagnosticoIMC where
      show IN = "Peso inferior al normal"
      show N  = "Peso nomal"
      show SN = "Peso superior al normal"
      show O  = "Obesidad"
data Paciente  = Paciente {cedulaP :: Int,nombreP :: String, apellidoP :: String, geneP:: Genero, pesoP :: Float, estaturaP :: Float,imcP :: Float, diagnosticoP :: DiagnosticoIMC}
instance Eq  Paciente where
      Paciente cedula _ _ _ _ _ _ _ == Paciente cedula2 _ _ _ _ _ _ _  = (cedula == cedula2)

instance Show Paciente where
      show (Paciente  cedula nombre apellido genero peso estatura imc diagnostico) = "\nCedula: " ++ show cedula ++  "\nNombre: " ++ nombre ++ "\nApellido: " ++ apellido ++ "\nGenero: " ++ show genero ++ "\nPeso: " ++ show peso ++ "\nEstatura: " ++ show estatura ++ "\nIMC: " ++ show imc ++ "\nDiagnostico: " ++ show diagnostico

imcCalcule :: Float -> Float -> Float
imcCalcule peso estatura =  peso / (estatura*estatura)
diagnosticoIMC:: Float -> DiagnosticoIMC
diagnosticoIMC imc
            | imc < 18.5 = IN
            | imc >= 18.5  && imc <25.0 = N
            | imc >= 25.0  && imc <30   = SN
            | otherwise =  O

-- funcion principalI
main:: IO ()
main = iterateForever menuOptions ['1' ..] menuAcions []
      where menuOptions = ["Agregar paciente","Buscar paciente", "Buscar alertas","Modificar Datos(Peso o estatura)","Eliminar paciente","Imprimir pacientes","Salir"]
            menuAcions = [agregarPaciente    , buscarPaciente  , buscarAlertas   , modificarPacienteIO              , eliminarPaciente   , imprimirPacientes, salirSuccess]


--------------- MENU ACTIONS ---------------

agregarPaciente:: [Paciente]  -> IO [Paciente] {- declaracion del tipo de la funcion, que resive y que retorna.
					                  en esto caso recive los pacientes y los retorna procesados-}
agregarPaciente pacientes = do
      putStrLn "Igrese la cedula: "
      cedula <- readLn

      --no existe el paciente
      if filtrer' pacientes (\p -> cedulaP p == cedula) == [] -- busca en los pacientes existentes uno con la cedula ingressada
            then do
                  pacient <- createPacient cedula
                  let pacientes' =  add' pacientes pacient
                  return pacientes' -- al ser la ultima accion IO el do la toma como valor de retorno
            else do
                        putStrLn "Ya existes en el sistema"
                        return pacientes -- al ser la ultima accion IO el do la toma como valor de retorno
buscarPaciente:: [Paciente]  -> IO [Paciente]
buscarPaciente pacientes = do
      putStrLn "Igrese la cedula del paciente: "
      cedula <-   readLn
      let pacientesFiltrados = filtrer' pacientes (\p -> cedulaP p == cedula) -- al evaluar comparar con un solo parametro esta retorna otra que resive un parametro menos
      if pacientesFiltrados == []
            then putStr "No existe paciente con esa cedula.\n"
            else print $ head' pacientesFiltrados

      return pacientes -- genera un IO pacientes, al ser el ultimo del do se retorna



buscarAlertas:: [Paciente]  -> IO [Paciente]
buscarAlertas pacientes = do
      if pacientesIMBajo == []
            then putStrLn $ "No hay pacientes con " ++ show IN
            else do
                  putStrLn $ "Pacientes con " ++ show IN
                  print pacientesIMBajo
      if pacientesIMAlto == []
            then putStrLn $ "No hay pacientes con " ++ show SN
            else do
                  putStrLn $ "Pacientes con " ++ show SN
                  print pacientesIMAlto

      if pacientesIMElevado == []
            then putStrLn $ "No hay pacientes con " ++ show O
            else do
                  putStrLn $ "Pacientes con " ++ show O
                  print pacientesIMElevado
      return pacientes                         ----------Fucion anonima que compara el diagnostico de una persona con el de otra-------------
      where pacientesIMBajo =  filtrer' pacientes (\p-> (diagnosticoP p) == IN )
            pacientesIMAlto =  filtrer' pacientes (\p-> (diagnosticoP p) == SN )
            pacientesIMElevado =  filtrer' pacientes (\p-> (diagnosticoP p) == O )

modificarPacienteIO:: [Paciente]  -> IO [Paciente]
modificarPacienteIO pacientes = do
      putStrLn "Igrese la cedula del paciente: "
      cedula <- readLn
      let pacientesFiltrados = filtrer' pacientes (\p -> cedulaP p == cedula) -- busca todos los pacientes que tengan la cedula cedula, estos pueden ser uno o cero
      if pacientesFiltrados == []
            then do -- si la funcion no encontro un paciente
                  print "No hay paciente con esa cedula."
                  return pacientes
            else do -- la funcion filtro algo, se sabe que maximo aparece un paciente ahi
                                                      -----obtengo ese paiente y lo mando como parametro a modificar -------
                  pacienteModificadoEstatura <- modificarEstatura (head' pacientesFiltrados) -- le dice a la funcion auxiliar que modifique al paciente
                  pacienteModificado <- modificarPeso(pacienteModificadoEstatura)
                  return (modifyElement pacientes  (\p -> if p == pacienteModificado then pacienteModificado else p))
            where modificarEstatura paciente@(Paciente cedula nombre apellido genero peso _ _ _) = do
                        putStr "Desea modificar Estatura[S/N]\n"
                        modificar <- getChar'
                        if (modificar == 'S')
                              then do
                                    nuevaEstatura <-  seleccionarEstatura
                                    let nuevoimc = imcCalcule peso nuevaEstatura
                                    return $ Paciente cedula nombre apellido genero peso nuevaEstatura nuevoimc (diagnosticoIMC nuevoimc)
                              else
                                    return paciente
                  modificarPeso paciente@(Paciente cedula nombre apellido genero _ estatura _  _) = do
                        putStr "Desea modificar Peso[S/N]\n"
                        modificar <- getChar'
                        if (modificar == 'S')
                              then do
                                    nuevoPeso <-  seleccionarPeso
                                    let nuevoimc = imcCalcule nuevoPeso estatura
                                    return $ Paciente cedula nombre apellido genero nuevoPeso estatura nuevoimc (diagnosticoIMC nuevoimc)
                              else
                                    return paciente

--MenuAction: elimina un paciente del sistema
eliminarPaciente:: [Paciente]  -> IO [Paciente]
eliminarPaciente pacientes = do
      putStrLn "Igrese la cedula del paciente: "
      cedula <- readLn :: IO Int
      let pacientes'  = filtrer' pacientes (\paciente -> cedulaP paciente  /= cedula)
      if len' pacientes == len' pacientes'
            then  putStr "No existe el pacciente con esa cedula"
            else  putStr "Exito, se ha eliminado el paciente"
      return pacientes'
imprimirPacientes:: [Paciente] -> IO [Paciente]
imprimirPacientes pacientes = do
      if pacientes == []
            then print "No hay pacientes en el sistema."
            else print pacientes
      return pacientes -- al ser la ultima accion IO el do la toma como valor de retorno


salirSuccess:: e -> IO e
salirSuccess _   = do
      putStr "Adios...\n"
      exitSuccess
--------------------- AUXILIAR IO_FUNCTIONS ---------------------
-- le pide los datos al usuario y crea un paciente
createPacient:: Int -> IO Paciente
createPacient cedula  = do
                  putStrLn "Igrese el nombre: "
                  nombre <- getLine
                  putStrLn "Igrese el apellido: "
                  apellido <- getLine
                  genero <- seleccionarGenero
                  peso <- seleccionarPeso
                  estatura <- seleccionarEstatura
                  let imc = imcCalcule peso estatura
                  return $ Paciente cedula nombre apellido genero peso estatura imc (diagnosticoIMC imc)
seleccionarPeso:: IO Float
seleccionarPeso = do
      putStrLn "Igrese el peso[Kg]: "
      peso <- readLn
      if  peso <= 0 || peso > 501 -- la persona mas obesa pesa 500 Kg
            then do
                  putStrLn "El peso debe estar entre 1 y 501 Kg"
                  seleccionarPeso
            else return peso
seleccionarEstatura:: IO Float
seleccionarEstatura = do
      putStrLn "Igrese el estatura[Metros]: "
      estatura <- readLn
      if  estatura <= 0 || estatura > 3 -- la persona mas alta mide 3 Metros
            then do
                  putStrLn "La esttura debe estar entre 1 y 3 Metros"
                  seleccionarEstatura
            else return estatura

-- le pide al usuario el genero y lo retorna
seleccionarGenero:: IO Genero
seleccionarGenero = do
    option <- selectOption options ['1' ..]
    case option of
        '1' -> return Masculino
        '2' -> return Femenino
        '3' -> return Otro
        _ -> do
              putStr "Opcion invalida mi pez \n"
              seleccionarGenero --esta funcion retorna IO Genero y al ser la ultima sentencia de este do se retorna ese valor
        where options = ["Hombre","Mujer","Otro"]

--Modifica un paciente pasado por parametro
{-
imprimirPaciente:: Paciente -> IO ()
imprimirPaciente (cedula,nombre,apellido,)-}



-------------------------------------    OPERACIONES SOBRE LISTAS ----------------------------------------------------
                                      ------ FUNCIONES PURAS.   ------
