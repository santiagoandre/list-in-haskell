all: main.hs
	ghc -o main main.hs
main: main.hs
	ghc -o main main.hs
clean:
	rm main
	rm main.o
	rm main.hi
	rm Menu.o
	rm Menu.hi
